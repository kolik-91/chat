'use strict';
(function () {
  var socket = io();   

  $('#form-message').submit(function(e){
    e.preventDefault();
    socket.emit('send message', $('#message').val());
    $('#message').val('');
    return false;
  });

  socket.on('new message', function(data){
    $('#messages').append('<li class="message-list"><strong> ' + data.user + '</strong>: ' + data.msg + '</li');
  });

  $('#formUser').submit(function(e){
    e.preventDefault();
    socket.emit('new user', $('#username').val(), function(data){
      if(data){
        $('#usersArea').hide();
        $('#messagesArea').show();
      }
    });
    $('#username').val();
  })

  socket.on('get users', function(data){
    var html = '';
    for (var i = 0; i < data.length; i++){
      html += '<li class="list-group-item">' + data[i] + '</li>'; 
    }
    $('#users').html(html);
  })

})();
